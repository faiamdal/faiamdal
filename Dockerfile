FROM alpine:latest

RUN mkdir -p /etc/caddy \
 && install -d /usr/share/caddy

ADD Caddyfile /etc/caddy/Caddyfile
ADD entrypoint.sh /opt/entrypoint.sh

RUN apk update \
 && apk upgrade \
 && apk add --no-cache ca-certificates curl wget tar grep sed busybox caddy \
 && chmod +x /opt/entrypoint.sh \
 && chmod +x /etc/caddy/Caddyfile

ENTRYPOINT ["sh", "-c", "/opt/entrypoint.sh"]
