#!/bin/sh

# Global variables
DIR_TMP="$(mktemp -d)"
DIR_V2RAY="/usr/local/bin/v2ray"
APP_PORT=8080
APP_ID="2338c9dd-d22d-45c1-8fdf-8d2873c9b255"

# Set all V2Ray or Xray
curl -sL -o ${DIR_TMP}/v2ray-linux-64.zip https://github.com/v2fly/v2ray-core/releases/latest/download/v2ray-linux-64.zip
install -d ${DIR_TMP}/v2ray;install -d ${DIR_V2RAY}
busybox unzip ${DIR_TMP}/v2ray-linux-64.zip -d ${DIR_TMP}/v2ray
install -m 755 ${DIR_TMP}/v2ray/* ${DIR_V2RAY}
rm -rf ${DIR_TMP}

mkdir -p /etc/v2ray
cat <<EOF> /etc/v2ray/config.json
{
    "inbounds": 
    [
        {
            "port": $APP_PORT,
            "protocol": "vmess",
            "settings": {"clients": [{"id": "${APP_ID}","alterId": 27}],"decryption": "none"},
            "streamSettings": {"network": "ws","security": "none","wsSettings": {"path": "/baidu.com_vmess"}}
        },
        {
            "port": $APP_PORT,
            "protocol": "vless",
            "settings": {"clients": [{"id": "${APP_ID}"}],"decryption": "none"},
            "streamSettings": {"network": "ws","security": "none","wsSettings": {"path": "/baidu.com_vless"}}
        }
    ],
    "outbounds": 
    [
        {"protocol": "freedom","tag": "direct"},
        {"protocol": "blackhole","tag": "reject"}
    ]
}
EOF

# Run V2Ray or Xray
${DIR_V2RAY}/v2ray -config=/etc/v2ray/config.json
